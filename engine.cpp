#include "engine.h"

#include <future>
#include <thread>
#include <functional>

namespace othello
{


bool OthelloGameEngine::initNewGame()
{
    clearGame();

    m_board.at(0).set(36);
    m_board.at(0).set(27);
    m_board.at(1).set(35);
    m_board.at(1).set(28);
    currentPlayer = PlayerId::Two;
    passed = false;

    return true;
}

void OthelloGameEngine::clearGame() {

    m_board.at(0).reset();
    m_board.at(1).reset();


}

bool OthelloGameEngine::performMoveForCurrentHuman(const BitPos& board_pos)
{
    if (utility::isLegalMove(board(), currentPlayerId(), board_pos)) {
        utility::placeAndFlip(m_board, currentPlayerId(), board_pos);
        return true;
    }
    else {
        return false;
    }
}

void OthelloGameEngine::performAIMove()
{
    BitPos pos;
    if (currentPlayerId()==othello::PlayerId::One)
    pos = m_player_one.obj->bestMove();
    else
    pos = m_player_two.obj->bestMove();
    utility::placeAndFlip(m_board, currentPlayerId(), pos);
}

void OthelloGameEngine::think(const std::chrono::seconds& time_limit)
{
    passedThisTurn(false);
    PlayerType playerType = currentPlayerType();
    PlayerId playerId = currentPlayerId();
    if (playerType == othello::PlayerType::Human) {
        return;
    }

    if (playerId == PlayerId::One){
      m_player_one.obj->think(board(), currentPlayerId(), time_limit);
    }
    else {
        m_player_two.obj->think(board(), currentPlayerId(), time_limit);
    }
}

PlayerId OthelloGameEngine::currentPlayerId() const
{
    return currentPlayer;
}

PlayerType OthelloGameEngine::currentPlayerType() const
{
    if (currentPlayer == PlayerId::One)
        return m_player_one.type;
    else
        return m_player_two.type;
}

BitPieces OthelloGameEngine::pieces(const PlayerId& player_id) const
{
    if ((int) player_id == 0)
        return m_board.at(0);
    else
        return m_board.at(1);
}

const BitBoard& OthelloGameEngine::board() const { return m_board; }

void OthelloGameEngine::switchPlayer()
{
    currentPlayer = utility::opponent(currentPlayer);
}

void OthelloGameEngine::initialInterface()
{
    m_board.at(0).set(63);

    std::bitset<64> initialState0 (std::string("01010000"
                                               "11000000"
                                               "00000000"
                                               "10010000"
                                               "00000000"
                                               "00000100"
                                               "00000000"
                                               "00000001"));

    std::bitset<64> initialState1 (std::string("10101000"
                                               "00000000"
                                               "10100000"
                                               "00000000"
                                               "10001000"
                                               "00000000"
                                               "00000010"
                                               "00000000"));
    m_board.at(0) = initialState0;
    m_board.at(1) = initialState1;

}

int OthelloGameEngine::checkMoves()
{
    PlayerId playerId = currentPlayerId();
    auto moves = utility::legalMoves(board(), playerId).size();

    return moves;
}

bool OthelloGameEngine::passedLastTurn()
{
    return passed;
}

void OthelloGameEngine::passedThisTurn(bool flag)
{
    if (flag)
        passed = true;
    else
        passed = false;
}

int OthelloGameEngine::getScore(PlayerId playerId)
{
    return utility::countingScore(playerId, m_board);
}

//void OthelloGameEngine::callFromThread()
//{
//    std::cout << "Hello, World" << std::endl;
//}

}   // namespace othello
