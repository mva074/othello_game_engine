#ifndef ENGINE_H
#define ENGINE_H

#include "utility.h"

#include <basic_types.h>
#include <engine_interface.h>
#include <orangemonkey_ai.h>
#include <iostream>



namespace othello
{

  class OthelloGameEngine : public othello::GameEngineInterface {

    // GameEngineInterface interface
  public:
    bool initNewGame() override;
    void clearGame() override;
    bool performMoveForCurrentHuman(const othello::BitPos&) override;
    void performAIMove();
    bool legalMovesCheck(const othello::BitPos&) const;

    void                think(const std::chrono::seconds&) override;
    othello::PlayerId   currentPlayerId() const override;
    othello::PlayerType currentPlayerType() const override;
    othello::BitPieces  pieces(const othello::PlayerId&) const override;

    const othello::BitBoard& board() const override;
    void switchPlayer();
    void initialInterface();
    int checkMoves();
    bool passedLastTurn();
    void passedThisTurn(bool flag);
    int getScore(othello::PlayerId playerId);
//    void static callFromThread();

  private:
    othello::PlayerId currentPlayer;
    bool passed;

  };



} // namespace othello
#endif   // ENGINE_H
