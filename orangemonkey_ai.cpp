#include "orangemonkey_ai.h"

#include <utility.h>
#include <windows.h>

using namespace std;


// stl
#include <iostream>

namespace othello::monkey_ais
{


  OrangeMonkeyAI::OrangeMonkeyAI(const PlayerId&) {
    m_engine.seed(m_rd());
  }

  void OrangeMonkeyAI::think(const othello::BitBoard& board,
                             const othello::PlayerId& player_id,
                             const std::chrono::seconds& max_time)
  {
    //std::cout << "orange monkey thinking!!" << std::endl;
    const auto legal_moves = utility::legalMoves(board, player_id);

    std::uniform_int_distribution<size_t> dist(0, legal_moves.size()-1);

    auto b_iter = std::begin(legal_moves);
    std::advance(b_iter, dist(m_engine));

    m_best_move = *b_iter;

//    auto time = max_time.count() * 500;

//    Sleep(time);
  }

  BitPos OrangeMonkeyAI::bestMove() const {

    const auto best_move = BitPos(m_best_move.value());
    m_best_move = BitPos::invalid();
    return best_move;
  }

}   // namespace othello::monkey_ais
