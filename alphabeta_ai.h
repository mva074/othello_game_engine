#ifndef ALPHABETA_AI_H
#define ALPHABETA_AI_H

#include <player_interface.h>
#include <random>
#include <vector>
#include <future>


namespace othello::alphabeta_ais
{
using BitBoardVector = std::vector<BitBoard>;

class AlphabetaAI : public AIInterface {

    // Constructors
public:
    AlphabetaAI(const PlayerId& player_id);

    // PlayerInterface interface
public:
    void   think(const BitBoard& board, const PlayerId& player_id,
                 const std::chrono::seconds& max_time);
    BitPos bestMove() const;
    int alphabeta(const BitBoard& board, const PlayerId& player_id, int depth, int alfa, int beta);
    BitBoardVector createTree(BitBoard& board, const PlayerId& player_id);
    int evaluate(const BitBoard& board, const PlayerId& player_id, const BitPos& board_pos);
    BitPos takeMoveFromTree(const BitBoard &init, const BitBoard &final);
    int takeMax(const std::vector<int> alphabetaValues);
    std::tuple<int, int> applyAlphabeta(const BitBoard init, const std::vector<BitBoard> tree, const PlayerId &player_id,
                       int depth, const std::chrono::seconds &max_time);

private:
    mutable BitPos     m_best_move;
    std::random_device m_rd;
    std::mt19937       m_engine;
};

}   // namespace othello::alphabeta_ais
#endif // ALPHABETA_AI_H

