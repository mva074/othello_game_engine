#include "utility.h"

// stl
#include "numeric"
#include "iostream"
#include <bitset>

namespace othello::utility
{
  ////////////////////
  //
  //
  // Interface Utility
  // Functions
  //
  //
  ////////////////////

  BitPieces occupiedPositions(const BitBoard& board)
  {
      BitPieces positionsOne = board.at(0);
      BitPieces positionsTwo = board.at(1);
      BitPieces positions = positionsOne | positionsTwo;
    return positions;
  }

  bool occupied(const BitPieces& pieces, const BitPos& board_pos)
  {
      if (board_pos.value() < 64)
      return pieces.test(board_pos.value());
      else
          return false;
  }

  bool occupied(const BitBoard& board, const BitPos& board_pos)
  {
      BitPieces pieces = occupiedPositions(board);
    return occupied(pieces, board_pos);
  }

  BitPos nextPosition(const BitPos& board_pos, const MoveDirection& dir)
  {    
    BitPos pos;
    bool flag = false;
    std::vector<MoveDirection> borderDirection;
    MoveDirection border = borderPos(board_pos);
//    int dirN = (int) dir;
    MoveDirection dirId = dir;
    if (border != MoveDirection::SE || board_pos.value() == 0){
        borderDirection = borderDir(border);
        for (auto border_direction : borderDirection){
            if (border_direction == dirId)
                flag = true;
        }
        if (flag == false)
        return board_pos;
    }
    switch (dir) {
    case MoveDirection::N : {
        pos = BitPos{board_pos.value() + 8};
        break;
    }
    case MoveDirection::S : {
        pos = BitPos{board_pos.value() - 8};
        break;
    }
    case MoveDirection::E : {
        pos = BitPos{board_pos.value() - 1};
        break;
    }
    case MoveDirection::W : {
        pos = BitPos{board_pos.value() + 1};
        break;
    }
    case MoveDirection::NE : {
        pos = BitPos{board_pos.value() + 7};
        break;
    }
    case MoveDirection::NW : {
        pos = BitPos{board_pos.value() + 9};
        break;
    }
    case MoveDirection::SE : {
        pos = BitPos{board_pos.value() - 9};
        break;
    }
    case MoveDirection::SW : {
        pos = BitPos{board_pos.value() - 7};
        break;
    }
    }
        if (pos.value() < 64)
            return pos;
        else
            return board_pos;
  }

  BitPos findBracketingPiece(const BitBoard& board,
                             const BitPos& board_pos,
                             const PlayerId& player_id,
                             const MoveDirection& dir)
  {
      BitPos nextPos = board_pos, nextPos2;
      bool isOcc = true, isOccPlayer = true;
      auto id = abs((int) player_id - 1);
      int count = 0;
      while (isOcc && isOccPlayer)
      {
              nextPos2 = nextPos;
              nextPos = nextPosition(nextPos, dir);
              if (nextPos2 != nextPos){
                  isOcc = occupied(board, nextPos);
                  isOccPlayer = occupied(board.at(id), nextPos);
                  count++;
              }
              else {
                  isOcc = false;
                  isOccPlayer = false;
              }
      }
          if (count == 1)
              return board_pos;
          else
              if (occupied(board.at((int) player_id), nextPos))
                  return nextPos;
              else
                  return board_pos;
  }

  BitPosSet legalMoves(const BitBoard& board, const PlayerId& player_id)
  {
      int j = 0, z, countMoves;
      MoveDirection direction[8], border;
      std::vector<MoveDirection> borderDirection;
      BitPieces occBoard = occupiedPositions(board);
      BitPosSet legalMoves;
      BitPos pos;
      for (int i = 0; i < 8; ++i){
          direction[i] = (MoveDirection)i;
      }

      while(j != 64){
          pos = BitPos{j};
          countMoves = 0;
          border = borderPos(pos);
          if (!occupied(occBoard, pos)) {
              //check if is not in the border
              if (border == MoveDirection::SE && pos.value() != 0){
                  for (z = 0; z < 8; z++) {
                      if (pos != findBracketingPiece(board, pos, player_id, direction[z]))
                          countMoves++;
                  }
              }
              else {
                  borderDirection = borderDir(border);
                  for (auto border_direction : borderDirection) {
                      if (pos != findBracketingPiece(board, pos, player_id, border_direction))
                          countMoves++;
                  }
              }
              if (countMoves != 0){
                  legalMoves.insert(pos);
              }
          }
          j++;
      }

    return legalMoves;
  }

  bool isLegalMove(const BitBoard& board, const PlayerId& player_id,
                   const BitPos& board_pos)
  {
      BitPosSet moves = legalMoves(board, player_id);
      bool response = moves.count(board_pos);
    return response;
  }


  void placeAndFlip(BitBoard& board, const PlayerId& player_id,
                    const BitPos& board_pos)
  {

      board.at((int) player_id).set(board_pos.value());

      BitPosSet bracketingPieces;
      BitPos piece, loopPos;
      MoveDirection direction[8];
      for (int i = 0; i < 8; ++i){
          direction[i] = (MoveDirection)i;
      }

      //flip
      for (int i = 0; i < 8; i++){
          //check the right directions for flip
          piece = findBracketingPiece(board, board_pos, player_id, direction[i]);
          if (piece != board_pos){
              loopPos = nextPosition(board_pos, direction[i]);
              while (piece != loopPos) {
                        board.at((int) player_id).set(loopPos.value());
                        board.at(abs((int) player_id - 1)).reset(loopPos.value());
                        loopPos = nextPosition(loopPos, direction[i]);
              }
          }

      }
  }

  PlayerId opponent(const PlayerId &currentPlayer)
  {
      if (currentPlayer == PlayerId::One)
          return PlayerId::Two;
      else
          return PlayerId::One;
  }

  MoveDirection borderPos(const BitPos &board_pos)
  {
      switch (board_pos.value()) {
      case 0:
          return MoveDirection::SE;
      case 7:
          return MoveDirection::SW;
      case 56:
          return MoveDirection::NE;
      case 63:
          return MoveDirection::NW;
      default:
          if (board_pos.value() < 7 && board_pos.value() > 0)
              return MoveDirection::S;
          if (board_pos.value() > 56 && board_pos.value() < 63)
              return MoveDirection::N;
          if (board_pos.value() % 8 == 0)
              return MoveDirection::E;
          if (board_pos.value() % 8 == 7)
              return MoveDirection::W;
          return MoveDirection::SE;
      }

  }

  int countingScore (const PlayerId &playerId, const BitBoard &board)
  {
      int score;
      if (playerId == PlayerId::One)
          score = board.at(0).count();
      else
          score = board.at(1).count();
      return score;
  }

  BitBoard placeAndFlip(const BitBoard &initialBoard, const BitPos &board_pos, const PlayerId &player_id)
  {
      BitBoard board = initialBoard;
      board.at((int) player_id).set(board_pos.value());

      BitPosSet bracketingPieces;
      BitPos piece, loopPos;
      MoveDirection direction[8];
      for (int i = 0; i < 8; ++i){
          direction[i] = (MoveDirection)i;
      }

      //flip
      for (int i = 0; i < 8; i++){
          //check the right directions for flip
          piece = findBracketingPiece(board, board_pos, player_id, direction[i]);
          if (piece != board_pos){
              loopPos = nextPosition(board_pos, direction[i]);
              while (piece != loopPos) {
                  board.at((int) player_id).set(loopPos.value());
                  board.at(abs((int) player_id - 1)).reset(loopPos.value());
                  loopPos = nextPosition(loopPos, direction[i]);
              }
          }


      }

      return board;
  }

  std::vector<MoveDirection> borderDir(const MoveDirection &dir)
  {
      std::vector<MoveDirection> borderDir;

      switch (dir) {
      case MoveDirection::N:
          borderDir.push_back(MoveDirection::W);
          borderDir.push_back(MoveDirection::SW);
          borderDir.push_back(MoveDirection::S);
          borderDir.push_back(MoveDirection::SE);
          borderDir.push_back(MoveDirection::E);
          break;
      case MoveDirection::S:
          borderDir.push_back(MoveDirection::W);
          borderDir.push_back(MoveDirection::NW);
          borderDir.push_back(MoveDirection::N);
          borderDir.push_back(MoveDirection::NE);
          borderDir.push_back(MoveDirection::E);
          break;
      case MoveDirection::E:
          borderDir.push_back(MoveDirection::W);
          borderDir.push_back(MoveDirection::SW);
          borderDir.push_back(MoveDirection::S);
          borderDir.push_back(MoveDirection::NW);
          borderDir.push_back(MoveDirection::N);
          break;
      case MoveDirection::W:
          borderDir.push_back(MoveDirection::N);
          borderDir.push_back(MoveDirection::NE);
          borderDir.push_back(MoveDirection::S);
          borderDir.push_back(MoveDirection::SE);
          borderDir.push_back(MoveDirection::E);
          break;
      case MoveDirection::NE:
          borderDir.push_back(MoveDirection::W);
          borderDir.push_back(MoveDirection::SW);
          borderDir.push_back(MoveDirection::S);
          break;
      case MoveDirection::NW:
          borderDir.push_back(MoveDirection::E);
          borderDir.push_back(MoveDirection::SE);
          borderDir.push_back(MoveDirection::S);
          break;
      case MoveDirection::SE:
          borderDir.push_back(MoveDirection::W);
          borderDir.push_back(MoveDirection::NW);
          borderDir.push_back(MoveDirection::N);
          break;
      case MoveDirection::SW:
          borderDir.push_back(MoveDirection::N);
          borderDir.push_back(MoveDirection::NE);
          borderDir.push_back(MoveDirection::E);
          break;
      }
      return borderDir;
  }

}   // namespace othello::utility
