#ifndef UTILITY_H
#define UTILITY_H

#include <utility_interface.h>
#include <vector>

namespace othello::utility
{
    PlayerId opponent (const PlayerId &currentPlayer);
    MoveDirection borderPos(const BitPos &board_pos);
    std::vector<MoveDirection> borderDir(const MoveDirection &dir);
    BitBoard placeAndFlip(const BitBoard &board, const BitPos &board_pos, const PlayerId &player_id);
    int countingScore (const PlayerId &playerId, const BitBoard &board);

}   // namespace othello::utility

#endif // UTILITY_H
