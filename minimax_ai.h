#ifndef MINIMAX_AI_H
#define MINIMAX_AI_H

#include <player_interface.h>
#include <random>
#include <algorithm>
#include <vector>
#include <thread>
#include <future>

namespace othello::minimax_ais
{
    using BitBoardVector = std::vector<BitBoard>;

  class MinimaxAI : public AIInterface {

    // Constructors
  public:
    MinimaxAI(const PlayerId& player_id);

    // PlayerInterface interface
  public:
    void   think(const BitBoard& board, const PlayerId& player_id,
                 const std::chrono::seconds& max_time);
    BitPos bestMove() const;
    int minimax(const BitBoard& board, const PlayerId& player_id, int depth);
    BitBoardVector createTree(BitBoard& board, const PlayerId& player_id);
    int evaluate(const BitBoard& board, const PlayerId& player_id, const BitPos& board_pos);
    BitPos takeMoveFromTree(const BitBoard &init, const BitBoard &final);
    int takeMax(const std::vector<int> minimaxValues);
     std::tuple<int, int> applyMinimax(const BitBoard init, const std::vector<BitBoard> tree, const PlayerId &player_id,
                     int depth, const std::chrono::seconds &max_time);

  private:
    mutable BitPos     m_best_move;
    std::random_device m_rd;
    std::mt19937       m_engine;
  };

}   // namespace othello::minimax_ais
#endif // MINIMAX_AI_H
