#include "alphabeta_ai.h"

#include <utility.h>

// stl
#include <limits>
#include <tuple>

// stl - cextentions
#include <cstddef>

using namespace std;

// stl
#include <iostream>
#include <iomanip>

namespace othello::alphabeta_ais {


AlphabetaAI::AlphabetaAI(const PlayerId&) {
    m_engine.seed(m_rd());
}

bool sortFunction (int i,int j) { return (i>j); }

void AlphabetaAI::think(const othello::BitBoard& board,
                      const othello::PlayerId& player_id,
                      const std::chrono::seconds& max_time)
{
    //std::cout << "alphabeta thinking!! " /*<< (max_time == 2s)*/ << std::endl;
    if (max_time == 2s)
        return;

    BitBoard init = board;
    BitBoardVector tree = createTree(init, player_id);
    std::vector<int> maxElementsIndexesDepthZero;
    std::vector<int> maxElementsIndexes;

    //depth zero alphabeta call
    std::vector<int> alphabetaValuesDepthZero;
    for (const auto &board_gen : tree){
        alphabetaValuesDepthZero.push_back(alphabeta(board_gen, player_id, 1, -10, 15));
    }

    // find the max value for depth zero
    int maxElementIndexDepthZero = takeMax(alphabetaValuesDepthZero);
    BitBoard finalDepthZero = tree[maxElementIndexDepthZero];
    othello::BitPos best_overall_move = takeMoveFromTree(init, finalDepthZero);

    int previousMax = alphabetaValuesDepthZero[maxElementIndexDepthZero];

        // Construct futures
        std::vector<std::future<std::tuple<int,int>>> futures;

        const auto function = std::mem_fn(&AlphabetaAI::applyAlphabeta);
        BitBoardVector firstBranchTree, secondBranchTree, thirdBranchTree;

        int branchLength = floor(size(tree)/3);
        //        int branchLength = floor(size(tree)/2);
        if (branchLength != 0) {
            copy(tree.begin(), tree.begin()+branchLength, back_inserter(firstBranchTree));
            copy(tree.begin()+branchLength, tree.end()-branchLength, back_inserter(secondBranchTree));
            copy(tree.end()-branchLength, tree.end(), back_inserter(thirdBranchTree));
            //            copy(tree.end()-branchLength, tree.end(), back_inserter(secondBranchTree));
        }
        else {
            copy(tree.begin(), tree.end(), back_inserter(firstBranchTree));
            copy(tree.begin(), tree.end(), back_inserter(secondBranchTree));
            copy(tree.begin(), tree.end(), back_inserter(thirdBranchTree));
        }

        for (int depth = 2; depth < 5; depth+=2) {
            futures.emplace_back(std::async( std::launch::async,
                                             function,
                                             this,
                                             init, firstBranchTree, player_id, depth, max_time ));
            futures.emplace_back(std::async( std::launch::async,
                                             function,
                                             this,
                                             init, secondBranchTree, player_id, depth, max_time ));
            futures.emplace_back(std::async( std::launch::async,
                                             function,
                                             this,
                                             init, thirdBranchTree, player_id, depth, max_time ));

        }

         auto max_val = previousMax/*std::numeric_limits<int>::min()*/;
         for( auto& future : futures ) {
             const auto [future_max, best_move]  = future.get();
             if(future_max > max_val) {
                 max_val = future_max;
                 best_overall_move = BitPos{best_move};
             }
         }
         m_best_move = best_overall_move;

}

int AlphabetaAI::evaluate(const BitBoard& board, const PlayerId& player_id, const BitPos& board_pos)
{
    int value = 0, highScore, highScore_after_move, flippedPieces;
    int player_one_score = utility::countingScore(PlayerId::One, board);
    int player_two_score = utility::countingScore(PlayerId::Two, board);
    othello::MoveDirection border = utility::borderPos(board_pos);
    // a move is a good move for many reasons, these reasons for my opinion

    // first reason, the piece is in the border or better in the corner
    switch (border) {
    case MoveDirection::NE:
        value = value + 5;
        break;
    case MoveDirection::NW:
        value = value + 5;
        break;
    case MoveDirection::SE:
        if (board_pos.value() == 0)
            value = value + 5;
        else
            value = value - 3;
        break;
    case MoveDirection::SW:
        value = value + 5;
        break;
    case MoveDirection::S:
        value = value + 2;
        break;
    case MoveDirection::N:
        value = value + 2;
        break;
    case MoveDirection::E:
        value = value + 2;
        break;
    case MoveDirection::W:
        value = value + 2;
        break;
    default:
        value = value - 3;
        break;
    }

    // second reason, how many pieces you will flip with this move?
    auto board_copy = utility::placeAndFlip(board, board_pos, player_id);
    int player_one_score_after_move = utility::countingScore(PlayerId::One, board_copy);
    int player_two_score_after_move = utility::countingScore(PlayerId::Two, board_copy);
    if (player_id == PlayerId::One)
        flippedPieces = player_one_score_after_move - player_one_score - 1;
    else
        flippedPieces = player_two_score_after_move - player_two_score - 1;
    value = value + flippedPieces;

    // third reason, change the score?
    highScore = max(player_one_score, player_two_score);
    highScore_after_move = max(player_one_score_after_move, player_two_score_after_move);

    if (player_id == PlayerId::One){
        if (player_one_score == player_two_score) {
            if (highScore_after_move == player_one_score_after_move)
                // if from draw to win
                value = value + 2;
        }
        else if (highScore == player_two_score){
            if (player_one_score_after_move == player_two_score_after_move){
                // if from lose to draw
                value = value + 3;
            }
            else if (highScore_after_move == player_two_score_after_move){
                // if from lose to lose
                value = value - 4;
            }
            else {
                // if from lose to win
                value = value + 6;
            }
        }
    }
    else {
        if (player_one_score == player_two_score) {
            if (highScore_after_move == player_two_score_after_move)
                // if from draw to win
                value = value + 2;
        }
        else if (highScore == player_one_score){
                if (player_one_score_after_move == player_two_score_after_move){
                    // if from lose to draw
                    value = value + 3;
                }
                else if (highScore_after_move == player_one_score_after_move){
                    // if from lose to lose
                    value = value - 4;
                }
                else {
                    // if from lose to win
                    value = value + 5;
                }
            }
        }

    // fourth reason, the move can carry in a situation that determine
    // the end of the game or no valid moves for the opponent

    auto legalMovesOpponent = 0, legalMovesAfter = 0;
    if (player_id == PlayerId::One) {
        legalMovesOpponent = utility::legalMoves(board_copy, PlayerId::Two).size();
        if (legalMovesOpponent == 0) {
            value = value + 5;
            legalMovesAfter = utility::legalMoves(board_copy, player_id).size();
            if (legalMovesAfter == 0) {
                // this position carries to the end of the match
                if (highScore == player_one_score_after_move){
                    value = value + 10;
                }
                else {
                    value = value - 10;
                }
            }
        }
    }
    else {
        legalMovesOpponent = utility::legalMoves(board_copy, PlayerId::One).size();
        if (legalMovesOpponent == 0) {
            value = value + 5;
            legalMovesAfter = utility::legalMoves(board_copy, player_id).size();
            if (legalMovesAfter == 0) {
                // this position carries to the end of the match
                if (highScore == player_two_score_after_move){
                    value = value + 10;
                }
                else {
                    value = value - 10;
                }
            }
        }
    }

    int occupiedPos = player_one_score_after_move + player_two_score_after_move;
    if (player_id == PlayerId::One) {
        if (occupiedPos > 63) {
            if (highScore == player_one_score_after_move)
                value = value + 5;
            else
                value = value - 7;
        }
        else {
            if (occupiedPos > 63){
                if (highScore == player_two_score_after_move)
                    value = value + 5;
                else
                    value = value - 7;
            }
        }
    }
    return value;
}

BitPos AlphabetaAI::takeMoveFromTree(const BitBoard &init, const BitBoard &final)
{
    BitPieces positionBefore = utility::occupiedPositions(init);
    BitPieces positionAfter = utility::occupiedPositions(final);
    BitPieces bestPosition = positionAfter & positionBefore.flip();
    for (int i = 0; i < 64; i++) {
        if (bestPosition[i] == 1)
            return BitPos{i};
    }
}

int AlphabetaAI::takeMax(const std::vector<int> alphabetaValues)
{
        std::vector<int> maxElementsIndexes;

        // take max element
        int maxElementIndex = std::max_element(alphabetaValues.begin(),alphabetaValues.end()) - alphabetaValues.begin();
//        for (int value : alphabetaValues)
//        std::cout << " " << value;
//        std::cout << std::endl;

        // more than a max ??
        if (std::binary_search (alphabetaValues.begin()+maxElementIndex+1,
                                alphabetaValues.end(), alphabetaValues[maxElementIndex])){
            maxElementsIndexes.push_back(maxElementIndex);
            std::vector<int> sortedAlphabetaValues(alphabetaValues);
            std::sort(sortedAlphabetaValues.begin(), sortedAlphabetaValues.end(), std::greater<>()); //sortFunction);
            auto i =  count(sortedAlphabetaValues.begin(), sortedAlphabetaValues.end(), alphabetaValues[maxElementIndex]);
            auto index = maxElementIndex;
            i--;
            // search all the max values
            while (i != 0){
                i--;
                auto iter = find(alphabetaValues.begin()+index+1, alphabetaValues.end(), alphabetaValues[maxElementIndex]);
                index = iter - alphabetaValues.begin();
                maxElementsIndexes.push_back(index);
            }

            // random choice between the max values
            std::uniform_int_distribution<size_t> dist(0, maxElementsIndexes.size()-1);
            auto b_iter = std::begin(maxElementsIndexes);
            std::advance(b_iter, dist(m_engine));
            maxElementIndex = *b_iter;
        }

        return maxElementIndex;

}

BitPos AlphabetaAI::bestMove() const {

    const auto best_move = BitPos(m_best_move.value());
    m_best_move = BitPos::invalid();
    return best_move;
}

int AlphabetaAI::alphabeta(const BitBoard &board, const PlayerId &player_id, int depth, int alpha, int beta)
{
    BitPosSet legal_moves = utility::legalMoves(board, player_id);
    int nmoves = legal_moves.size();
    BitPosSet legal_moves_opp = utility::legalMoves(board, utility::opponent(player_id));
    int nmovesOpp = legal_moves_opp.size();
    depth = depth - 1;
    BitBoard newBoard;
    int value, score = alpha;

    if (nmoves == nmovesOpp && nmoves == 0)
        return alpha;
    if (depth%2 == 0) {
        // player_id's turn
        if (depth == 0) { // reached max depth of search
            //              BitBoard *boards = createTree(board, player_id);
            for (auto move : legal_moves){
                value = evaluate(board, player_id, move);
                if (value > score) {
                    score = value;
                }
                if (score > beta) {
                    return score;
                }
            }
            return score;
        }
        else{
            if (nmoves == 0) {
                value = alphabeta(board, utility::opponent(player_id), depth, alpha, beta);
            }
            for (auto move : legal_moves){
                newBoard = utility::placeAndFlip(board, move, player_id);
                value = alphabeta(newBoard, player_id, depth, alpha, beta);
                if (value > score){
                    score = value;
                }
                if (score > beta) {
                    return score;
                }
            }
            return score;
        }
    }
    else {
        //opponent's turn
        if (nmovesOpp == 0) {
            value = alphabeta(board, player_id, depth, alpha, beta);
        }
        for (auto move : legal_moves_opp){
            newBoard = utility::placeAndFlip(board, move, utility::opponent(player_id));
            value = alphabeta(newBoard, utility::opponent(player_id), depth, alpha, beta);
            if (value > score) {
                score = value;
            }
            if (score > beta) {
                return score;
            }
        }

        return score;
    }
}

BitBoardVector AlphabetaAI::createTree(BitBoard &board, const PlayerId &player_id)
    {
        BitBoardVector results;
        BitPosSet possibleMoves = utility::legalMoves(board, player_id);
        for (auto move : possibleMoves) {
            results.push_back(utility::placeAndFlip(board, move, player_id));
        }
        return results;
    }

std::tuple<int, int> AlphabetaAI::applyAlphabeta(const BitBoard init, const std::vector<BitBoard> tree,
                                                             const PlayerId &player_id, int depth, const std::chrono::seconds &max_time)
{

    const auto time_start = std::chrono::steady_clock::now();
    std::vector<int> alphabetaValues;
    for (const auto &board_gen : tree){
        alphabetaValues.push_back(alphabeta(board_gen, player_id, depth, -10, 12));
        const auto time_check = std::chrono::steady_clock::now();
        if (const auto duration = std::chrono::duration_cast<std::chrono::seconds>(time_check - time_start);
            duration >= max_time)
          break;
    }

//    for (int value : alphabetaValues) {
//        std::cout << value << " ";
//    }
//    std::cout << "value at depth " << depth << std::endl;

    int maxElementIndex = takeMax(alphabetaValues);

//    if (previousMax <= alphabetaValues[maxElementIndex]) {
    BitBoard final = tree[maxElementIndex];
    BitPos move = takeMoveFromTree(init, final);
//    }

    std::tuple<int, int> t = std::make_tuple(alphabetaValues[maxElementIndex], move.value());

    return t;
}


}
// namespace othello::alphabeta_ais

